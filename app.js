const fs = require('fs'); // package per la gestione del file system
const express = require('express');
const bodyParser = require('body-parser');
const path = require('path');
// Importiamo il files di routing
const placesRoutes = require('./routes/places-routes');
const usersRoutes = require('./routes/users-routes');

const mongoose = require('mongoose');

const HttError = require('./models/http-error');

const app = express();

// Effettua un parse di tutte le richieste in entrata in formato Json e passa al prossimo Middleware
app.use(bodyParser.json()); // aggiunge la proprietà request.body

// *** Middleware per upload delle immagini ***
// express.static => ritorna un nuovo middleware per restituire le immagini da un url
app.use('/uploads/images', express.static(path.join('uploads', 'images'))); // path.join => unisce i due path

// *** Occore aggiungere un Header a tutte le response per evitare problemi di CORS ***
app.use((req, res, next) => {
  // Consente a tutti i domini di inviare richieste
  res.setHeader('Access-Control-Allow-Origin', '*'); // con '* chiunque può accedere al server localhost:5000
  // Si specificano i tipi headers possono accedere
  res.setHeader(
    'Access-Control-Allow-Headers',
    'Origin, X-Requested-With, Content-Type, Accept, Authorization'
  );
  // Si specificano i tipi di metodi sono concessi
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PATCH, DELETE');

  next();
});

// Registriamo i Middleware, per i routes
app.use('/api/places', placesRoutes); // => /api/places...
app.use('/api/users', usersRoutes); // => /api/users...

// Middleware viene eseguito se non si trova il route
app.use((req, res, next) => {
  const error = new HttError('Could not find this route', 404);
  throw error;
});

// Gestione ERRORE che arriva da Middleware precedente
app.use((error, req, res, next) => {
  if (req.file) {
    // nella request c'è un file
    fs.unlink(req.file.path, (err) => {
      // il file viene rimosso
      console.log(err);
    });
  }

  if (res.headerSent) {
    // la response è stata inviata
    return next(error);
  }
  // Si controlla se c'è un errore, diversamente si imposta 500 e descrizione generica
  res.status(error.code || 500);
  res.json({ message: error.message || 'An unknown error occurred!' });
});
// Connesione al database di Mongo('mern' come indicato nella stringa) con Mongoose
mongoose
  .connect(
    /* 'process' è una variabile globale di NodeJS env viene iniettata durante l'esecuzione e prende
      i valori dal file nodemon.json */
    `mongodb+srv://${process.env.DB_USER}:${process.env.DB_PASSWORD}@cluster0-afsqd.mongodb.net/${process.env.DB_NAME}?retryWrites=true&w=majority`,
    { useNewUrlParser: true, useUnifiedTopology: true, useCreateIndex: true }
  )
  .then(() => {
    app.listen(process.env.PORT || 5000); // se è prevista una porta in caso di deploy su un server verrà usata quella
  })
  .catch((error) => {
    console.log(error);
  });

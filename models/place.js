// Creazione dello Schema richiesto da Mongoose per accedere al database
const mongoose = require('mongoose');

const Schema = mongoose.Schema; // schema di mongoose

const placeSchema = new Schema({
  title: { type: String, required: true },
  description: { type: String, required: true },
  image: { type: String, required: true },
  address: { type: String, required: true },
  location: {
    lat: { type: Number, required: true },
    lng: { type: Number, required: true }
  },
  // Si specifica che il creator è un campo di tipo _id che proviene dallo schema 'User'
  creator: { type: mongoose.Types.ObjectId, required: true, ref: 'User' }
});
// Per convenzione il nome deve essere singolare e con la lettere maiuscola(Mongoose poi lo convertirà in plurale)
module.exports = mongoose.model('Place', placeSchema);
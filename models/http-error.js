class HttpError extends Error {
    constructor(message, errorCode) {
        super(message); // Sostituiisce il valore della proprietà "message" della classe Error
        this.code = errorCode;
    }
}

module.exports = HttpError;

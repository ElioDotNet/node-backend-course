const mongoose = require('mongoose');
const uniqueValidator = require('mongoose-unique-validator');

const Schema = mongoose.Schema;
// Schema per gli Users
const userSchema = new Schema({
  name: { type: String, required: true },
  email: { type: String, required: true, unique: true },
  password: { type: String, required: true, minlength: 6 },
  image: { type: String, required: true },
  // Collegamento con i place: relazione uno a molti(per quello che si usano parentesi [])
  places: [{ type: mongoose.Types.ObjectId, required: true, ref: 'Place' }]
});
// Si controlla che il campo definito unique non abbia dupplicati
userSchema.plugin(uniqueValidator);
// Per convenzione il nome deve essere singolare e con la lettere maiuscola(Mongoose poi lo convertirà in plurale)
module.exports = mongoose.model('User', userSchema);

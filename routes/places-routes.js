const express = require('express');
// Si usa per la validazione dell'input
const { check } = require('express-validator');

const placeController = require('../controllers/places-controller');
// Serve per upload dei file usando Multer
const fileUpload = require('../middleware/file-upload');
// Verifica validità Token
const checkAuth = require('../middleware/check-auth');

const router = express.Router();

/*** GESTIONE DEL ROUTING(ATTENZIONE L'ORDINE DEI ROUTE E' IMPORTANTE!!) ***/
router.get('/:pid', placeController.getPlaceById); // si imposta un puntatore alla funzione

router.get('/user/:uid', placeController.getPlacesByUserId); // si imposta un puntatore alla funzione
// Verifica token
router.use(checkAuth); // i router successivi saranno eseguiti SOLO sel il token è valido
// Si possono inserire più Middleware e verranno valutati da sinistra a destra
router.post(
  '/',
   // Viene attaccato il singolo file, con la chiave specificata(image) che verrà utilizzata nel frontend
   fileUpload.single('image'),
  [
    check('title')
      .not()
      .isEmpty(),
    check('description').isLength({ min: 5 }),
    check('address')
      .not()
      .isEmpty()
  ],
  placeController.createPlace // si imposta un puntatore alla funzione
);

router.patch(
  '/:pid',
  [
    check('title')
    .not()
    .isEmpty(),
    check('description').isLength({ min: 5 })
  ],
  placeController.updatePlace); // si imposta un puntatore alla funzione

router.delete('/:pid', placeController.deletePlace); // si imposta un puntatore alla funzione

module.exports = router;

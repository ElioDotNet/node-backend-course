const express = require('express');
// Serve per la validazione dell'input
const { check } = require('express-validator');

const usersController = require('../controllers/users-controller');
// Serve per upload dei file usando Multer
const fileUpload = require('../middleware/file-upload');

const router = express.Router();

/*** GESTIONE DEL ROUTING(ATTENZIONE L'ORDINE DEI ROUTE E' IMPORTANTE!!) ***/
router.get('/', usersController.getUsers); // si imposta un puntatore alla funzione

router.post(
    '/signup',
    // Viene attaccato il singolo file, con la chiave specificata(image) che verrà utilizzata nel frontend
    fileUpload.single('image'),
    [
        check('name')
            .not()
            .isEmpty(),
        check('email').normalizeEmail() // Test@test.com => test@test.com
            .isEmail(),
        check('password').isLength({min: 5})
    ],
    usersController.signup); // si imposta un puntatore alla funzione

router.post('/login', usersController.login); // si imposta un puntatore alla funzione

module.exports = router;

const uuid = require('uuid/v1');
const multer = require('multer');

const MIME_TYPE_MAP = {
    'image/png': 'png',
    'image/jpeg': 'jpeg',
    'image/jpg': 'jpg'
}

const fileUpload = multer({
    limits: 500000, // limite dimensione file in kbyte
    storage: multer.diskStorage({
        destination: (req, file, cb) => { // request,file,callback
            cb(null, 'uploads/images'); // path dove salvare le immagini
        },
        filename: (req, file, cb) => {
            const ext = MIME_TYPE_MAP[file.mimetype]; // restituisce estensione del file
            cb(null, uuid() + '.' + ext); // genera un id a cui si concatena l'estensione del file
        }
    }),
    // Si controlla se il formato del file è valido
    fileFilter: (req, file, cb) => {
        const isValid = !!MIME_TYPE_MAP[file.mimetype]; // converte il risultato in un boolean
        const error = isValid ? null: new Error('Invalid mime type!');
        cb(error, isValid);
    }
});

module.exports = fileUpload;
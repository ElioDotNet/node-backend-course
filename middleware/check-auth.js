/* MIDDLEWARE PER VALIDARE LE RICHIESTE PER SPECIFICO TOKEN */
const jwt = require('jsonwebtoken');
const HttpError = require('../models/http-error');

module.exports = (req, res, next) => {
  if (req.method === 'OPTIONS') {
    // quando c'è una request con questo metodo non occore verificare il token
    return next();
  }
  try {
    // Il token si trova nell'header della request in Authorization
    const token = req.headers.authorization.split(' ')[1]; // Authorization: 'Bearer TOKEN' il token è il 2° elemento
    if (!token) {
      throw new Error('Authentication failed!');
    }
    // Si verifica la validità del token
    const decodedToken = jwt.verify(token, process.env.JWT_KEY); // se il token non è valido viene generato un errore
    req.userData = { userId: decodedToken.userId }; // i middleware successivi possono utilizzare userId della request
    next();
  } catch {
    const error = new HttpError('Authentication failed!', 403);
    return next(error);
  }
};

const axios = require('axios');

const HttpError = require('../models/http-error');

// Funzione per ottenere le coordinate in formato corretto
async function getCoordsForAddress(address) {
  // *** Dati Fake nel caso in cui non si dispone di un account Google ***
  return {
    lat: 40.7484474,
    lng: -73.9871516
  };

  // *** Dati reali se si dispone di un account Google ***
  // const response = await axios.get(
  //   `https://maps.googleapis.com/maps/api/geocode/json?address=${encodeURIComponent(
  //     address
  //   )}&key=${process.env.GOOGLE_API_KEY}`
  // );

  // const data = response.data;

  // if (!data || data.status === 'ZERO_RESULTS') {
  //   const error = new HttpError(
  //     'Could not find location for the specified address.',
  //     422
  //   );
  //   throw error;
  // }

  // const coordinates = data.results[0].geometry.location;

  // return coordinates;
}

module.exports = getCoordsForAddress;

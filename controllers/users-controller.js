const HttpError = require('../models/http-error');

const { validationResult } = require('express-validator');

const User = require('../models/user');
const bcrypt = require('bcryptjs'); // si usa per criptare/decriptare una password
const jwt = require('jsonwebtoken'); // si usa per la generazione del token

// Lettura Users
const getUsers = async (req, res, next) => {
  let users;
  try {
    /* find non restituisce una promise se interessa basta aggiunge .exec()
     Se si usa find().populate si usa una instance di una Query, non serve exec perchè chiamato indirettamente da async/await */
    users = await User.find({}, '-password'); // restituisce tutti i campi esclusa la password
  } catch {
    const error = new HttpError('Fetching users faild, please try again', 500);
    return next(error);
  }
  // Con 'getters: true' si converte l'oggetto di Mongoose in oggetto javascript, con un campo id in formato stringa
  res.json({ users: users.map(user => user.toObject({ getters: true }))});
};

// Creazione nuovo User
const signup = async (req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) { // ci sono errori di validazione
   return  next(new HttpError('Invalid inputs passed, please check your data.', 422));
  }

  const { name, email, password } = req.body;

  let existingUser;

  try {
    existingUser = await User.findOne({ email });
  } catch(error) {
    return next(error); // necessario per interrompere l'esecuzione
  }

  if (existingUser) {
    const error = new HttpError('User already exist, please login instead', 422);
    return next(error);
  }

  let hashedPassword;

  try {
    hashedPassword = await bcrypt.hash(password, 12); // livello di sicurezza password
  } catch  {
    const error = new HttpError('Could not create user, please try again', 500);
    return next(error);
  }

  const createdUser = new User({
    name,
    email,
    image: req.file.path, // inseriamo il path del file che ci arriva dalla request
    password: hashedPassword, // password criptata
    places: [] // si usa un array perchè possiamo avere tanti places
  });

  // Salviamo i dati in Mongo
  try {
    await createdUser.save();
  } catch (error) {
    return next(error); // necessario per interrompere l'esecuzione
  }

  // Generazione Token(inseriamo i dati che ci interessano)
  let token;
  try {
    token = jwt.sign({
      userId: createdUser.id,
      email: createdUser.email
    }, process.env.JWT_KEY, { expiresIn: '1h'}); // parola segreta e scadenza token
  } catch (error) {
    return next(error); // necessario per interrompere l'esecuzione
  }

  // Il token viene restuituito al chiamante più altri parametri che ci interessano
  res.status(201).json({ userId: createdUser.id, email: createdUser.email, token});
};

const login = async (req, res, next) => {
  const { email, password } = req.body;

  let existingUser;

  try {
    existingUser = await User.findOne({ email });
  } catch(error) {
    return next(error);
  }

  if (!existingUser) {
    const error = new HttpError('Invalid credentials, could not log in', 403);
    return next(error);
  }

  let isValidPassword = false;
  try {
    // Si confronta la password nella request con la password in Mongo decriptata
    isValidPassword = await bcrypt.compare(password, existingUser.password);
  } catch (err) {
    const error = new HttpError(
      'Could not log you in, please check your credentials and try again.',
      500
    );
    return next(error);
  }

  // Password non valida
  if (!isValidPassword) {
    const error = new HttpError('Invalid credentials, could not log in', 403);
    return next(error);
  }

  // Generazione Token(inseriamo i dati che ci interessa inserire)
  let token;
  try {
    token = jwt.sign(
      { userId: existingUser.id, email: existingUser.email },
      // Parola segreta e scadenza token
      process.env.JWT_KEY,
      { expiresIn: '1h' }
    );
  } catch {
    const error = new HttpError(
      'Logging in failed, please try again later.',
      500
    );
    return next(error);
  }

  // Il token viene restuituito al chiamante più altri parametri che ci interessano
  res.status(201).json({ userId: existingUser.id, email: existingUser.email, token});
};

exports.getUsers = getUsers;
exports.signup = signup;
exports.login = login;

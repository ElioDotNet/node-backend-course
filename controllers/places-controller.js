// Importiamo la classe http-error
const HttError = require("../models/http-error");
const mongoose = require('mongoose');
// Controllo la validazione dell'input
const { validationResult } = require("express-validator");

const fs = require('fs');

const getCoordsForAddress = require("../util/location");

const Place = require('../models/place');
const User = require('../models/user');

// Filtro places tramite Id
const getPlaceById = async (req, res, next) => {
  // Questo path va aggiunto a quello specificato nel middelware(app.use)
  const placeId = req.params.pid; // va usato lo stesso nome del parametro passato nel route
  let place;

  try {
    place = await Place.findById(placeId); // non restituisce una promise se interessa basta aggiunge .exec(callback);
  } catch {
    const error = new HttError('Something went wrong. Could not found a place', 500);
    return next(error); // interrompe esecuzione programma
  }

  if (!place) {
    const error = new HttError('Could not find place for the provided Id.', 404);
    return next(error)  // interrompe esecuzione programma
  }
  // Con 'getters: true' si converte l'oggetto di Mongoose in oggetto javascript, con un campo id in formato stringa
  await res.json({ place: place.toObject({ getters: true }) });
};

// Filtro places tramite UserId
const getPlacesByUserId = async (req, res, next) => {
  const userId = req.params.uid;
  let userWithPlaces;

  try {
    // Accedo alla collection dei places dello user con uno specifico id
    userWithPlaces = await User.findById(userId).populate('places'); // non restituisce una promise se interessa basta aggiunge .exec(callback);
  } catch {
    const error = new HttError('Something went wrong. Could not fetching places', 500);
    return next(error)
  }

  if (!userWithPlaces || userWithPlaces.places.length === 0) {
    return next(
      new HttError('Could not find a places for the provided user id.', 404)
    );
  }
  // Con 'getters: true' si converte l'oggetto di Mongoose in oggetto javascript, con un campo id in formato stringa
  await res.json({ places: userWithPlaces.places.map(place => place.toObject({ getters: true}))});
};

// Creazione Place
const createPlace = async (req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    // Ci sono errori di validazione
    return next(new HttpError('Invalid inputs passed, please check your data.', 422)); // quando usiamo async si chiama next
  }

  const { title, description, address } = req.body;

  let coordinates;

  try {
    coordinates = await getCoordsForAddress(address); // le coordinate si ottengono dall'indirizzo
  } catch (error) {
    return next(error);
  }

  // Le proprietà devo essere le stesse di quelle impostate nello schema
  const createdPlace = new Place({
    title,
    description,
    address,
    location: coordinates,
    image: req.file.path, // inseriamo il path del file che ci arriva dalla request
    creator: req.userData.userId // userId ci viene restituito dal check-uuth
  });

  let user;

  try {
    user = await User.findById(req.userData.userId); // cerchiamo negli user lo specifico userId, per un determinatato place
  } catch(error) {
    return next(error); // necessario per interrompere l'esecuzione
  }

  if (!user) { // non si è trovato lo user
    const error = new HttError('Could not found user for provided id', 401);
    return next(error);
  }

  try {
    // Con le trasaction la collection dei place NON viene creata in automatico, quindi bisogna crearla da MondoDb Atlas
    const sess = await mongoose.startSession(); // apriamo la sessione
    sess.startTransaction(); // apriamo la transazione
    await createdPlace.save({ session: sess }); // salviamo il dato in Mongo
    user.places.push(createdPlace); // si aggiunge il place id appena creato allo user
    await user.save({ session: sess }); // aggiorniamo lo user con il nuovo place id appena creato
    await sess.commitTransaction() // commitiamo i dati solo se tutto è andato ok e salvataggio effettivo
  } catch(error) {
    // In caso di errore Mongo fa automaticamente una rollback
    return next(error); // necessario per interrompere l'esecuzione
  }

  // Con 'getters: true' si converte l'oggetto di Mongoose in oggetto javascript, con un campo id in formato stringa
  res.status(201).json({ place: createdPlace.toObject({ getters: true })});
};

// Aggiornamento Place
const updatePlace = async (req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    // Ci sono errori di validazione
    return (new HttpError('Invalid inputs passed, please check your data.', 422));
  }

  const { title, description } = req.body;
  const placeId = req.params.pid; // va usato lo stesso nome del parametro passato nel route
  let place;

  try {
    place = await Place.findById(placeId); // non restituisce una promise se interessa basta aggiunge .exec()
  } catch(error) {
    return next(error); // interrompe esecuzione programma
  }

  // Controllo che lo useId nel nella request, sia lo stesso che ha creato il place
  if (place.creator.toString() !== req.userData.userId) {
    const error = new HttpError(
      'You are not allowed to edit this place.',
      401
    );
    return next(error);
  }

  place.title = title;
  place.description = description;

  // Salviamo i dati in Mongo
  try {
    place.save();
  } catch(error) {
    return next(error); // interrompe esecuzione programma
  }

  // Con 'getters: true' si converte l'oggetto di Mongoose in oggetto javascript, con un campo id in formato stringa
  res.status(200).json({ place: place.toObject({ getters: true })});
};

// Cancellazione Place
const deletePlace = async (req, res, next) => {
  const placeId = req.params.pid; // va usato lo stesso nome del parametro passato nel route
  let place;

  try {
    // Si cerca il place nello specifico user che lo usa(si può fare solo se abbiamo specificato una relazione con ref)
    place = await Place.findById(placeId).populate('creator'); // non restituisce una promise se interessa basta aggiunge .exec(callback);
  } catch(error) {
    return next(error);
  }

  if (!place) {
    const error = new HttError('Could not find a place with this Id', 404);
    return next(error);
  }

  // Se user corrente è diverso da quello del place si genera un errore
  if (place.creator.id !== req.userData.userId) {
    const error = new HttpError(
      'You are not allowed to delete this place.',
      401
    );
    return next(error);
  }

  const imagePath = place.image;

  try {
    const sess = await mongoose.startSession(); // apriamo la sessione
    sess.startTransaction(); // apriamo la transazione
    await place.remove({ session: sess}); // rimuove il dato da Mongo
    place.creator.places.pull(place) // si rimuove l'id nell'array degli user
    await place.creator.save({ session: sess}); // salviamo lo user a cui si è rimosso il place
    await sess.commitTransaction() // commitiamo i dati solo se tutto è andato ok e salvataggio effetivo
  } catch(error) {
    // In caso di errore Mongo fa automaticamente una rollback
    return next(error); // interrompe esecuzione programma
  }

  // Cancellazione dell'immagine del place
  fs.unlink(imagePath, ( (err) => {
    console.log(err);
  }));

  res.status(200).json({ message: 'Deleted place' });
};

// Per esportare più funzioni anzichè usare module.exports si usa questa sintassi
exports.getPlaceById = getPlaceById;
exports.getPlacesByUserId = getPlacesByUserId;
exports.createPlace = createPlace;
exports.updatePlace = updatePlace;
exports.deletePlace = deletePlace;
